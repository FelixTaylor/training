package main.input;

public enum ClickType {
    UNKNOWN,
    LEFT,
    RIGHT,
    SCROLL
}
