package main.vertex;

import main.Display;

import java.awt.*;

import static java.lang.Math.*;

public class VertexConverter {
    public static double scale = 1;
    private static double zoomfactor = 1.2;

    public static Point convert(final Vertex vertex) {
        double x3d = vertex.getY() * scale;
        double y3d = vertex.getZ() * scale;
        double depth = vertex.getX() * scale;
        double[] val = scale(x3d, y3d, depth);
        int x = (int) (Display.WIDTH / 2 + val[0]);
        int y = (int) (Display.HEIGHT / 2 - val[1]);
        return new Point(x, y);
    }

    private static double[] scale(double x3d, double y3d, double depth) {
        double distance = sqrt(x3d * x3d + y3d * y3d);
        double theta = atan2(y3d, x3d);
        double depth2 = 15 - depth;
        double localScale = abs(1400/(depth2+1400));
        distance *= localScale;
        double[] out = new double[2];
        out[0] = distance * cos(theta);
        out[1] = distance * sin(theta);
        return out;
    }

    public static void rotateX(Vertex vertex, double degrees,  boolean clockWise) {
        double radius = sqrt(vertex.getY() * vertex.getY() + vertex.getZ() * vertex.getZ());
        double theta = atan2(vertex.getZ(), vertex.getY());

        theta += 2* PI/360 * degrees * (clockWise?-1:1);
        vertex.setY(radius * Math.cos(theta));
        vertex.setZ(radius * Math.sin(theta));
    }

    public static void rotateY(Vertex vertex, double degrees,  boolean clockWise) {
        double radius = sqrt(vertex.getX() * vertex.getX() + vertex.getZ() * vertex.getZ());
        double theta = atan2(vertex.getX(), vertex.getZ());

        theta += 2* PI/360 * degrees * (clockWise?-1:1);
        vertex.setX(radius * Math.sin(theta));
        vertex.setZ(radius * Math.cos(theta));
    }

    public static void rotateZ(Vertex vertex, double degrees,  boolean clockWise) {
        double radius = sqrt(vertex.getY() * vertex.getY() + vertex.getX() * vertex.getX());
        double theta = atan2(vertex.getY(), vertex.getX());

        theta += 2* PI/360 * degrees * (clockWise?-1:1);
        vertex.setY(radius * Math.sin(theta));
        vertex.setX(radius * Math.cos(theta));
    }


    public static void zoomOut() {
        scale /= zoomfactor;
    }

    public static void zoomIn() {
        scale *= zoomfactor;
    }
}
