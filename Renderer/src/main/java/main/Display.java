package main;

import main.input.ClickType;
import main.input.Mouse;
import main.shapes.Poly;
import main.shapes.Tetrahedron;
import main.vertex.Vertex;
import main.vertex.VertexConverter;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.Objects;

import static java.awt.Color.*;
import static java.lang.String.*;
import static java.util.Objects.*;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;
import static main.input.ClickType.*;

public class Display extends Canvas implements Runnable {
    private final static String title = "Renderererer";

    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;

    private Thread thread;
    private final JFrame frame;

    private boolean running = false;

    // todo refactor: move out
    private Tetrahedron tetrahedron;
    private Mouse mouse;

    private int initialX = 0, initialY = 0;

    public Display() {
        this.frame = new JFrame();
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        this.mouse = new Mouse();
        this.addMouseListener(this.mouse);
        this.addMouseMotionListener(this.mouse);
        this.addMouseWheelListener(this.mouse);
    }

    public synchronized void start() {
        this.running = true;
        this.thread = new Thread(this, "Display");
        this.thread.start();
    }

    public synchronized void stop() {
        this.running = false;
        try {
            this.thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        final double ns = 100_000_000.0 / 3;
        long lastTime = System.nanoTime();
        long timer = System.currentTimeMillis();
        double delta = 0;
        int frames = 0;

        init();

        while (running) {
            final long now = System.nanoTime();
            delta += (now-lastTime) / ns;
            lastTime = now;

            while (delta >= 1) {
                update();
                delta--;
                render();
                frames++;
            }


            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                this.frame.setTitle(title + " (" + frames + "fps)");
                frames = 0;
            }
        }
        stop();
    }

    private void init() {
        int size = 200;
        Vertex v1 = new Vertex(size/2, -size/2, -size/2);
        Vertex v2 = new Vertex(size/2, size/2, -size/2);
        Vertex v3 = new Vertex(size/2, size/2, size/2);
        Vertex v4 = new Vertex(size/2, -size/2, size/2);
        Vertex v5 = new Vertex(-size/2, -size/2, -size/2);
        Vertex v6 = new Vertex(-size/2, size/2, -size/2);
        Vertex v7 = new Vertex(-size/2, size/2, size/2);
        Vertex v8 = new Vertex(-size/2, -size/2, size/2);
        this.tetrahedron = new Tetrahedron(YELLOW,
                new Poly(BLUE,    v5,v6,v7,v8),
                new Poly(GREEN,   v1,v2,v6,v5),
                new Poly(YELLOW,  v1,v5,v8,v4),
                new Poly(CYAN,    v2,v6,v7,v3),
                new Poly(MAGENTA, v4,v3,v7,v8),
                new Poly(RED,     v1,v2,v3,v4)
        );
    }

    private void render() {
        final BufferStrategy strategy = this.getBufferStrategy();
        if (isNull(strategy)) {
            this.createBufferStrategy(3);
            return;
        }

        final Graphics graphics = strategy.getDrawGraphics();
        graphics.setColor(new Color(150,150,150));
        graphics.fillRect(0,0, WIDTH, HEIGHT);

        tetrahedron.render(graphics);
        graphics.dispose();
        strategy.show();
    }

    private void update() {
        int x = this.mouse.getMouseX();
        int y = this.mouse.getMouseY();
        if(this.mouse.getMouseButton() == LEFT) {
            int xdif = x - initialX;
            int ydif = y - initialY;
            this.tetrahedron.rotate(true, 0,-ydif/2.5,-xdif/2.5);
        }

        if (this.mouse.isScrollingUp()) {
            VertexConverter.zoomIn();
        } else if (this.mouse.isScrollingDown()){
            VertexConverter.zoomOut();
        }
        this.mouse.resetScroll();

        initialX = x;
        initialY = y;
    }

    public static void main(String[] args) {
        final Display display = initDisplay();
        display.start();
    }

    private static Display initDisplay() {
        Display display = new Display();
        display.frame.setTitle(title);
        display.frame.add(display);
        display.frame.pack();
        display.frame.setDefaultCloseOperation(EXIT_ON_CLOSE );
        display.frame.setLocationRelativeTo(null);
        display.frame.setResizable(false);
        display.frame.setVisible(true);
        return display;
    }
}
