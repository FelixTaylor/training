package main.shapes;

import main.vertex.Vertex;
import main.vertex.VertexConverter;

import java.awt.*;
import java.util.*;

import static java.util.Arrays.*;

public class Poly {
    private final Vertex[] vertices;
    private Color color;

    public static Poly[] sort(Poly... polygons) {
        final ArrayList<Poly> polyList = new ArrayList<>(asList(polygons));
        Collections.sort(polyList, new Comparator<Poly>() {
            @Override
            public int compare(Poly p1, Poly p2) {
                if (p2.getAverageX() == p1.getAverageX()) {
                    return 0;
                }
                return p2.getAverageX() - p1.getAverageX() < 0 ? 1 : -1 ;
            }
        });

        for (int i = 0; i < polygons.length; i++) {
            polygons[i] = polyList.get(i);
        }

        return polygons;
    }

//    public Poly(final Vertex... vertices) {
//        this.color = Color.WHITE;
//        this.vertices = new Vertex[vertices.length];
//        // important to do a deep copy
//        for (int i = 0; i < vertices.length; i++) {
//            final Vertex v = vertices[i];
//            this.vertices[i] = new Vertex(v.getX(), v.getY(), v.getZ());
//        }
//    }

    public Poly(final Color color, final Vertex... vertices) {
        this.color = color;
        this.vertices = new Vertex[vertices.length];
        // important to do a deep copy
        for (int i = 0; i < vertices.length; i++) {
            final Vertex v = vertices[i];
            this.vertices[i] = new Vertex(v.getX(), v.getY(), v.getZ());
        }
    }

    public double getAverageX() {
        double sum = 0;
        for (Vertex v : this.vertices) {
            sum += v.getX();
        }

        return sum / this.vertices.length;
    }

    public void render(Graphics graphics) {
        final Polygon polygon = new Polygon();
        for (Vertex vertex : this.vertices) {
            final Point p = VertexConverter.convert(vertex);
            polygon.addPoint(p.x, p.y);
        }
        graphics.setColor(this.color);
        graphics.fillPolygon(polygon);
    }

    public void rotate(boolean clockWise, double degreeX, double degreeY, double degreeZ) {
        for (Vertex v : this.vertices) {
            VertexConverter.rotateX(v, degreeX, clockWise);
            VertexConverter.rotateY(v, degreeY, clockWise);
            VertexConverter.rotateZ(v, degreeZ, clockWise);
        }
    }
//    public void setColor(final Color color) {
//        this.color = color;
//    }
}
