package main.shapes;

import java.awt.*;

public class Tetrahedron {
    private Poly[] polygons;

    public Tetrahedron(Color color, Poly... polygons) {
        this.polygons = polygons;
    }

    public Tetrahedron(Poly... polygons) {
        this.polygons = polygons;
        this.setPolygonColor();
    }

    private void setPolygonColor() {
//        for (Poly poly: this.polygons) {
//            poly.setColor(this.color);
//        }
    }

    public void render(final Graphics graphics) {
        for (Poly poly : this.polygons) {
            poly.render(graphics);
        }
    }

    public void rotate(boolean clockWise, double degreeX, double degreeY, double degreeZ) {
        for (Poly p : this.polygons) {
            p.rotate(clockWise, degreeX, degreeY, degreeZ);
        }
        this.sortPolygons();
    }

    private void sortPolygons() {
        Poly.sort(this.polygons);
    }



}
