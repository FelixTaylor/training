package main.immutables;

import org.immutables.value.Value;

@Value.Immutable
public abstract class Person {
    public abstract long getId();
    public abstract String getName();
    public abstract boolean getReal();
}
