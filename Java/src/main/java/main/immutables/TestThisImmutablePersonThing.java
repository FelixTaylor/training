package main.immutables;

public class TestThisImmutablePersonThing {
    public static void main(String[] args) {
        final Person person = ImmutablePerson.builder()
                .id(123456)
                .name("Felix")
                .real(true)
                .build();

        ImmutablePerson.copyOf(person).withName("");
        System.out.println(person);
    }
}
