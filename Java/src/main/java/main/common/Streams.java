package main.common;

import static java.util.Arrays.stream;

public class Streams {

    public void streamOverIntegers(final int... ints) {
        stream(ints).filter(i -> i > 2).forEach(System.out::println);
    }

}
