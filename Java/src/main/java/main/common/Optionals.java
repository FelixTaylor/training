package main.common;

import java.util.Optional;
import java.util.Random;

import lombok.NonNull;

import static java.util.Optional.empty;
import static java.util.Optional.of;

public class Optionals {

    public static void main(String[] args) {
        new Optionals();
    }

    private static class ResultStruct {
        public static String someVal = "This is a result";
    }

    public Optionals() {
        run();
    }

    public void run() {
        optionalOrNotOptional().ifPresentOrElse(
                resultStruct -> printToConsole(ResultStruct.someVal),
                () -> printToConsole("NO NO NO!"));
    }

    private Optional<ResultStruct> optionalOrNotOptional() {
        return new Random().nextInt(2) + 1 == 2
                ? of(new ResultStruct())
                : empty();
    }

    private void printToConsole(final @NonNull String msg) {
        System.out.println(msg);
    }

}
