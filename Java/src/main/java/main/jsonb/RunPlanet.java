package main.jsonb;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.JsonbBuilder;

public class RunPlanet {

    public static void main(String[] args) {
        singlePlanet();
        System.out.println("-------------------");
        planetList();
    }

    private static void singlePlanet() {
        final var jsonb = JsonbBuilder.create();
        final var resultJson = jsonb.toJson(Planet.builder()
                .name("Felix Centauri")
                .moons(1)
                .build());

        System.out.println(resultJson);
        final var obj = jsonb.fromJson(resultJson, Planet.class);
        System.out.println(obj.toString());
    }

    private static void planetList() {
        final var jsonb = JsonbBuilder.create();
        final var resultJson = jsonb.toJson(
                List.of(Planet.builder()
                                .name("Felix Centauri")
                                .moons(1)
                                .build(),
                        Planet.builder()
                                .name("Jabadaja")
                                .moons(2)
                                .build()));

        System.out.println(resultJson);
        final var obj = jsonb.fromJson(resultJson, new ArrayList<Planet>() {}.getClass().getGenericSuperclass());
        System.out.println(obj.toString());
    }
}
