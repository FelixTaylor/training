package vavr;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.jupiter.api.Test;

import io.vavr.control.Try;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VavrTest {
    // 1) todo: Lazy Methods -> https://www.baeldung.com/vavr#lazy

    public Integer divide(final @NonNull Integer dividend, final @NonNull Integer divisor) {
        // Hier geht an sich noch viel mehr, bpsw. Kontrollstrukturen die üblicherweise bei try-catch zum Einsatz
        // kommen, '.andFinally' aber auch '.andThen'
        return Try.of(() -> dividend / divisor).getOrElseThrow(() -> new ArithmeticException("Math error..."));
    }

    public Integer divideJ8(final @NonNull Integer dividend, final @NonNull Integer divisor) {
        return Optional.of(dividend / divisor).orElseThrow(() -> new ArithmeticException("Math error..."));
    }

    @Test
    void divide() {
        final var result = divide(10,3);
        assertEquals(3, result);
    }

}