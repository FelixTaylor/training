package abbreviation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Word_a10n {

    private final Abbreviator abbreviator = new Abbreviator();

    @Test
    public void testInternationalization() {
        assertEquals("i18n", abbreviator.abbreviate("internationalization"));
    }

    @Test
    public void testA11y2() {
        assertEquals("a11y", abbreviator.abbreviate("accessibility"));
    }
    @Test
    public void testA11y1() {
        assertEquals("A11y", abbreviator.abbreviate("Accessibility"));
    }
    @Test
    public void testElefant() {
        assertEquals("e5t-r2e", abbreviator.abbreviate("elefant-ride"));
    }

//    private String abbreviate(final String input) {
////        final int removed = input.length() - 2;
////        return "" + input.charAt(0) + removed + input.charAt(input.length()-1);
//
//
//        Arrays.stream(input.split("\\W"))
//                .filter(in -> in.length() > 3)
//                .forEach(in -> System.out.println(format("%s%d%s",in.charAt(0), in.length() - 2,in.charAt(input.length()-1))));
//
//
////        final int removed = input.length() - 2;
////        System.out.println("" + input.charAt(0) + removed + input.charAt(input.length()-1));
//        return "";
//    }

}
