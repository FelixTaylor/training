package abbreviation;

import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class Abbreviator {
    public static final Pattern PATTERN = Pattern.compile("([a-zA-Z]+)([\\W0-9]*)");

    public String abbreviate(final String text) {
        final var matcher = PATTERN.matcher(text);
        return matcher.results()
                .map(mr -> abbreviateOne(mr.group(1)) + mr.group(2))
                .collect(joining());
    }

    private String abbreviateOne(final String word) {
        final var length = word.length();
        return length > 3
                ? format("%s%d%s", word.charAt(0), length - 2, word.charAt(length - 1))
                : word;
    }
}
