package codewars;

import org.apache.commons.math3.util.Combinations;
import org.junit.jupiter.api.Test;

import static java.util.Spliterator.SORTED;
import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.stream.StreamSupport.stream;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PositionAverage {
    public static double posAverage(String s) {
        final String[] numbers = s.split(", ");

        Combinations c = new Combinations(numbers.length,2);
        stream(spliteratorUnknownSize(c.iterator(), SORTED), false);




//
//        double commonPosition = 0;
//        for(String first : numbers) {
//
//            for(String second : numbers) {
//                if(Objects.equals(first, second)) {
//                    continue;
//                }
//                for(int k=0; k<first.length();k++) {
//                    if(Objects.equals(first.toCharArray()[k], second.toCharArray()[k])) {
//                        commonPosition++;
//                    }
//                }
//            }
//        }
//        return (commonPosition / (numbers[0].length() * numbers.length)) *100;
        return 0;
    }


    private static void assertFuzzy(String s, double exp){
        System.out.println("Testing " + s);
        boolean inrange;
        double merr = 1e-9;
        double actual = PositionAverage.posAverage(s);
        inrange = Math.abs(actual - exp) <= merr;
        if (inrange == false) {
            System.out.println("Expected mean must be near " + exp +", got " + actual);
        }
        assertEquals(true, inrange);
        assertEquals(true, inrange);
    }

    @Test
    public void test() {
//        assertFuzzy("6900690040, 4690606946, 9990494604", 26.6666666667);
        assertFuzzy("466960, 069060, 494940, 060069, 060090, 640009, 496464, 606900, 004000, 944096", 26.6666666667);
//        assertFuzzy("444996, 699990, 666690, 096904, 600644, 640646, 606469, 409694, 666094, 606490", 29.2592592593);
//        assertFuzzy("4444444, 4444444, 4444444, 4444444, 4444444, 4444444, 4444444, 4444444", 100);
//        assertFuzzy("0, 0, 0, 0, 0, 0, 0, 0", 100);
    }

}
