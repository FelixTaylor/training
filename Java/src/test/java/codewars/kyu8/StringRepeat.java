package codewars.kyu8;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringRepeat {
    private static class Solution {
        public static String repeatStr(final int repeat, final String string) {
            return string.repeat(repeat);
        }
    }
    @Test
    public void test4a() {
        assertEquals("aaaa", Solution.repeatStr(4, "a"));
        assertEquals("HelloHelloHello", Solution.repeatStr(3, "Hello"));
        assertEquals("", Solution.repeatStr(5, ""));
        assertEquals("", Solution.repeatStr(0, "kata"));
    }

}
