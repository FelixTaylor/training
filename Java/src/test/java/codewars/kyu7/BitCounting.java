package codewars.kyu7;

import org.junit.jupiter.api.Test;

import static java.lang.Integer.toBinaryString;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BitCounting {
    public static int countBits(int n) {
        return toBinaryString(n).replace("0", "").length();
//        return Integer.bitCount(n); :)
    }

    @Test
    public void testGame() {
        assertEquals(5, BitCounting.countBits(1234));
        assertEquals(1, BitCounting.countBits(4));
        assertEquals(3, BitCounting.countBits(7));
        assertEquals(2, BitCounting.countBits(9));
        assertEquals(2, BitCounting.countBits(10));
    }
}
