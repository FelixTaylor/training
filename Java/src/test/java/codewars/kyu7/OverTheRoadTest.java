package codewars.kyu7;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OverTheRoadTest {
    public static long overTheRoad(long address, long n) {
        return n * 2 - (address - 1);
    }

    @Test
    void basicTestCases() {
        assertEquals(6, overTheRoad(1, 3));
        assertEquals(4, overTheRoad(3, 3));
        assertEquals(5, overTheRoad(2, 3));
        assertEquals(8, overTheRoad(3, 5));
        assertEquals(16, overTheRoad(7, 11));
        assertEquals(1999981L, overTheRoad(20, 1000000));
        assertEquals(596421736780L, overTheRoad(23633656673L, 310027696726L));
    }
}
