package codewars.kyu7;

import org.junit.jupiter.api.Test;

import static io.vavr.collection.Stream.*;
import static io.vavr.collection.Stream.rangeClosed;
import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SumOfNumbers {

    // Given two integers a and b, which can be positive or negative, find the sum of all the numbers between
    // including them too and return it. If the two numbers are equal return a or b.
//    public int sumOfNumbers(int a, int b) {
//        int sum = 0;
//        for (int i = min(a, b); i <= max(a, b); i++)
//            sum += i;
//
//        return sum;
//    }

    // Better
    public int sumOfNumbers(int a, int b) {
        return rangeClosed(min(a, b), max(a, b)).sum().intValue();
    }

    public int improvedSumOfNumbers(int a, int b) {
        return (a + b) * (abs(a - b) + 1) / 2;
    }

    @Test
    public void test1() {
        assertEquals(-1, sumOfNumbers(0, -1));
    }

    @Test
    public void test2() {
        assertEquals(1, sumOfNumbers(0, 1));
    }

    @Test
    public void testEqualAAndB() {
        assertEquals(2, sumOfNumbers(2, 2));
    }
}
