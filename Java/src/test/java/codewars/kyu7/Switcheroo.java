package codewars.kyu7;

import java.util.function.Function;
import java.util.regex.MatchResult;

import org.junit.jupiter.api.Test;

import lombok.NonNull;

import static java.util.regex.Pattern.compile;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Switcheroo {
    class Switch {
        public static String switcheroo(final @NonNull String x) {
            final Function<MatchResult, String> DO_IT = g -> g.group().equals("a") ? "b" : "a";
            return compile("(a|b)").matcher(x).replaceAll(DO_IT);
        }
    }

    @Test
    void testSomething() {
        assertEquals("abc", Switch.switcheroo( "bac"));
        assertEquals("ccc", Switch.switcheroo("ccc"));
        assertEquals("aaabcccbaaa", Switch.switcheroo("bbbacccabbb"));
    }
}
