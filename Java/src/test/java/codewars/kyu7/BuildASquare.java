package codewars.kyu7;

import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.rangeClosed;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BuildASquare {
    private static class Kata {
//        public static String generateShape(int n) {
//            final StringBuilder out = new StringBuilder();
//            rangeClosed(0, n - 1).forEach(num -> rangeClosed(0, n - 1)
//                    .forEach(s -> out.append(s == n - 1 ? "+\n" : "+")));
//            return out.toString().trim();
//        }

        // Better!!
//        public static String generateShape(int n) {
//            return IntStream.range(0, n)
//                    .mapToObj(c -> "+".repeat(n))
//                    .collect(joining("\n"));
//        }

        // EVEN Better!!
        public static String generateShape(int n) {
            return ("+".repeat(n) + "\n").repeat(n).trim();
        }
    }

    @Test
    public void exampleTests() {
        assertEquals("+++\n+++\n+++", Kata.generateShape(3));
        assertEquals("+++++\n+++++\n+++++\n+++++\n+++++", Kata.generateShape(5));
        assertEquals("++++++++\n++++++++\n++++++++\n++++++++\n++++++++\n++++++++\n++++++++\n++++++++", Kata.generateShape(8));
    }
}
