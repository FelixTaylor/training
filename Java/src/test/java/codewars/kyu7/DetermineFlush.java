package codewars.kyu7;

import java.util.function.Function;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.jupiter.api.Test;

import static java.util.Arrays.stream;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DetermineFlush {
    private static final Function<String, Character> TO_LAST_CHAR = c -> c.charAt(c.length()-1);

    public static boolean checkIfFlush(final @NonNull String[] cards){
      return stream(cards).map(TO_LAST_CHAR).distinct().count() == 1;
    }

    @Test
    public void BasicTests() {
        assertTrue(checkIfFlush(new String[]{"AS", "3S", "9S", "KS", "4S"}));
        assertFalse(checkIfFlush(new String[]{"AD", "4S", "7H", "KC", "5S"}));
        assertFalse(checkIfFlush(new String[]{"AD", "4S", "10H", "KC", "5S"}));
        assertTrue(checkIfFlush(new String[]{"QD", "4D", "10D", "KD", "5D"}));
    }
}
