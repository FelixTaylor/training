package codewars.kyu7;

import java.util.Objects;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import static java.util.stream.IntStream.range;
import static java.util.stream.IntStream.rangeClosed;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class RowWeights {
//    private static final Function2<int[], Integer, Integer> SUM = (w, b) -> rangeClosed(0, w.length-1)
//            .filter(i -> Objects.equals(i % 2, b))
//            .map(i -> w[i])
//            .sum();

    private static int sum(final int[] weights, final int b) {
        return range(0, weights.length)
                .filter(i -> Objects.equals(i % 2, b))
                .map(i -> weights[i])
                .sum();
    }

    public class Solution {
        public static int[] rowWeights (final int[] weights) {
//            return new int[]{SUM.apply(weights, 0), SUM.apply(weights, 1)};
//            return IntStream.rangeClosed(0,1).forEach(i -> sum(weights,i));
            return new int[]{sum(weights, 0), sum(weights, 1)};
        }
    }

    @Test
    public void Basic_Tests()
    {
        assertArrayEquals(new int[]{80,0}, Solution.rowWeights(new int[]{80}));
        assertArrayEquals(new int[]{100,50}, Solution.rowWeights(new int[]{100,50}));
        assertArrayEquals(new int[]{120,140}, Solution.rowWeights(new int[]{50,60,70,80}));
    }
    @Test
    public void Odd_Vector_Length()
    {
        assertArrayEquals(new int[]{62,27}, Solution.rowWeights(new int[]{13,27,49}));
        assertArrayEquals(new int[]{236,92}, Solution.rowWeights(new int[]{70,58,75,34,91}));
        assertArrayEquals(new int[]{211,164}, Solution.rowWeights(new int[]{29,83,67,53,19,28,96}));
    }
    @Test
    public void Even_Vector_Length()
    {
        assertArrayEquals(new int[]{100,50}, Solution.rowWeights(new int[]{100,50}));
        assertArrayEquals(new int[]{150,151}, Solution.rowWeights(new int[]{100,51,50,100}));
        assertArrayEquals(new int[]{207,235}, Solution.rowWeights(new int[]{39,84,74,18,59,72,35,61}));
    }

}
