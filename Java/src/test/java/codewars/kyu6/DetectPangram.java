package codewars.kyu6;

import org.junit.jupiter.api.Test;

import static java.lang.String.join;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DetectPangram {

    @Test
    void test1() {
        final String pangram1 = "The quick brown fox jumps over the lazy dog.";
        assertTrue(check(pangram1));
    }

    @Test
    void test2() {
        final String pangram2 = "You shall not pass!";
        assertFalse(check(pangram2));
    }

    boolean check(final String input) {
        final StringBuilder out = new StringBuilder();
        input.toLowerCase().chars()
                .filter(Character::isLetter)
                .distinct()
                .sorted()
                .mapToObj(c -> (char) c)
                .collect(toList())
                .forEach(out::append);

        return join("", out.toString())
                .equals("abcdefghijklmnopqrstuvwxyz");

    }

//    Bessere Loesung
    boolean check2(final String sentence) {
        return sentence.chars()
                .filter(Character::isLetter)
                .map(Character::toLowerCase)
                .distinct()
                .count() == 26;
    }
}
