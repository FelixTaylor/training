package codewars.kyu6;

import org.junit.jupiter.api.Test;

import static java.lang.Character.digit;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PersistentBugger {
    class Persist {
         static int persistence(final long n) {
            return crossProduct(n, 0);
        }

        private static int crossProduct(long n, int counter) {
            if (n < 10)
                return counter;
            final int res = Long.toString(n).chars()
                    .map(c -> digit(c, 10))
                    .reduce(1, (subtotal, e) -> subtotal * e);
            return crossProduct(res, ++counter);
        }
    }

    @Test
    void BasicTests() {
        System.out.println("****** Basic Tests ******");
        assertEquals(3, Persist.persistence(39));
        assertEquals(0, Persist.persistence(4));
        assertEquals(2, Persist.persistence(25));
        assertEquals(4, Persist.persistence(178126));
    }
}
