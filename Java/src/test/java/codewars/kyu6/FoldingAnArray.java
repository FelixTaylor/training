package codewars.kyu6;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FoldingAnArray {

    class Kata {
        static int[] foldArray(int[] array, int runs) {
            if (Objects.equals(runs, 0)) {
                return array;
            }

            final boolean isEven = array.length % 2 == 0;
            final int halfOfLength = array.length / 2;
            int[] out = new int[isEven ? halfOfLength : halfOfLength + 1];
            if (!isEven) {
                out[out.length-1] = array[halfOfLength];
            }

            IntStream.range(0, halfOfLength).forEach(i -> out[i] = array[i] + array[(array.length-1) -i]);
            return foldArray(out, runs -1);
        }
    }

    @Test
    void basicTests() {
        int[] input = new int[] { 1, 2, 3, 4, 5 };
        int[] expected = new int[] { 6, 6, 3 };
        assertEquals(Arrays.toString(expected), Arrays.toString(Kata.foldArray(input, 1)));

        expected = new int[] { 9, 6 };
        assertEquals(Arrays.toString(expected), Arrays.toString(Kata.foldArray(input, 2)));

        expected = new int[] { 15 };
        assertEquals(Arrays.toString(expected), Arrays.toString(Kata.foldArray(input, 3)));

        input = new int[] { -9, 9, -8, 8, 66, 23 };
        expected = new int[] { 14, 75, 0 };
        assertEquals(Arrays.toString(expected), Arrays.toString(Kata.foldArray(input, 1)));
    }

}
