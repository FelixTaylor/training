package codewars.kyu6;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.IntStream;
import static java.lang.String.*;

import static java.util.Arrays.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreatePhoneNumber {

    public static String createPhoneNumber(final int[] numbers) {
        return format("(%d%d%d) %d%d%d-%d%d%d%d", IntStream.of(numbers).boxed().toArray());
    }

    @Test
    void tests() {
        assertEquals("(123) 456-7890", createPhoneNumber(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0}));
    }
}
