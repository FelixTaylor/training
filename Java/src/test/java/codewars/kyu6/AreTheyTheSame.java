package codewars.kyu6;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import static java.util.Arrays.stream;
import static java.util.Objects.isNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AreTheyTheSame {
    public static boolean comp(int[] a, int[] b) {
        if (isNull(a) || isNull(b)) {
            return false;
        }
        var sorted = stream(a)
                .map(i -> i*i)
                .sorted()
                .toArray();
        return Arrays.equals(sorted, stream(b).sorted().toArray());
    }

    @Test
    public void test1() {
        int[] a = new int[]{121, 144, 19, 161, 19, 144, 19, 11};
        int[] b = new int[]{121, 14641, 20736, 361, 25921, 361, 20736, 361};
        assertTrue(AreTheyTheSame.comp(a, b));
    }

    @Test
    public void test6() {
        int[] a = new int[]{121, 144, 19, 161, 19, 144, 19, 11, 1008};
        int[] b = new int[]{121, 14641, 20736, 36100, 25921, 361, 20736, 361};
        assertTrue(AreTheyTheSame.comp(a, b));
    }
}
