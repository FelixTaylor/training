package com.example.demo.main.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.web.servlet.MockMvc;

import com.example.demo.main.model.CustomerEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CustomerControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void testAdd() {
        final var version = this.restTemplate.postForEntity(
                "/api/customer/add",
                new CustomerEntity().name("MaybeCustomer"),
                CustomerEntity.class);
    }

    @Test
    void listCustomers() throws Exception {
        final var version = this.restTemplate.getForObject("/api/customer/all", String.class);
        System.out.println(version);
    }
}