package com.example.demo.main.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.main.model.CustomerEntity;

import reactor.test.StepVerifier;

class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository repository;

    @Test
    public void whenDeleteAll_then0IsExpected() {
        repository.deleteAll()
                .as(StepVerifier::create)
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    public void whenInsert6_then6AreExpected() {
        insertCustomer();
        repository.findAll()
                .as(StepVerifier::create)
                .expectNextCount(5)
                .verifyComplete();
    }


    private void insertCustomer() {
        repository.save(new CustomerEntity().name("Hans"));
        repository.save(new CustomerEntity().name("Marie"));
        repository.save(new CustomerEntity().name("Ingo"));
        repository.save(new CustomerEntity().name("Jasmine"));
        repository.save(new CustomerEntity().name("James"));
    }
}