package com.example.demo.main.service;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.example.demo.main.model.CustomerEntity;
import com.example.demo.main.model.CustomerRequest;
import com.example.demo.main.model.CustomerResponse;
import com.example.demo.main.repo.CustomerRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.data.domain.ExampleMatcher.matchingAny;

@Service
public class CustomerService {

    private final CustomerRepository repository;

    public CustomerService(final @NonNull CustomerRepository repository) {
        this.repository = repository;
    }

    private CustomerResponse mapToResponse(final @NonNull CustomerEntity entity) {
        return new CustomerResponse(entity.id(), entity.name());
    }

    public Flux<CustomerResponse> search(final @NonNull CustomerRequest customer) {
        final var probe = new CustomerEntity().id(customer.id()).name(customer.name());
        final var matcher = matchingAny().withIgnoreCase("name");
        final var example = Example.of(probe, matcher);
        return repository.findAll(example).map(this::mapToResponse);
    }

    public Flux<CustomerResponse> findAll() {
        return repository.findAll().map(this::mapToResponse);
    }

    public Mono<CustomerEntity> save(final @NonNull CustomerRequest entity) {
        return repository.save(new CustomerEntity().name(entity.name()));
    }

    public Flux<CustomerResponse> findCustomerByName(final @NonNull String name) {
        return repository.findCustomerByName(name).map(this::mapToResponse);
    }

    public Mono<CustomerResponse> findCustomerEntityById(long id) {
        return repository.findCustomerEntityById(id).map(this::mapToResponse);
    }

    public Mono<CustomerResponse> findCustomerEntityByIdAndName(long id, final @NonNull String name) {
        return repository.findCustomerEntityByIdAndName(id, name).map(this::mapToResponse);
    }
}
