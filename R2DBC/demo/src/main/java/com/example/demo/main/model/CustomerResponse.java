package com.example.demo.main.model;

public record CustomerResponse(
        long id,
        String name
) {

}
