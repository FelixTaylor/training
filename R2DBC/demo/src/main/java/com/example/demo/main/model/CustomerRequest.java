package com.example.demo.main.model;

public record CustomerRequest (
        long id,
        String name
){

}
