package com.example.demo.main.controller;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.main.model.CustomerEntity;
import com.example.demo.main.model.CustomerRequest;
import com.example.demo.main.model.CustomerResponse;
import com.example.demo.main.service.CustomerService;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping(path = { "/api/customer" }, produces = APPLICATION_JSON_VALUE)
public class CustomerController {

    private final CustomerService service;

    public CustomerController(final @NonNull CustomerService service) {
        this.service = service;
    }

    @GetMapping("/all")
    public Flux<CustomerResponse> listCustomers() {
        return service.findAll();
    }

    @PostMapping("/add")
    public Mono<CustomerEntity> add(final @NonNull @RequestBody CustomerRequest c) {
        return service.save(c);
    }

    @GetMapping("/findByName/{name}")
    public Flux<CustomerResponse> findByName(final @NonNull @PathVariable String name) {
        return service.findCustomerByName(name);
    }

    @GetMapping("/findById/{id}")
    public Mono<CustomerResponse> findById(@PathVariable long id) {
        return service.findCustomerEntityById(id);
    }

    @GetMapping("/findByIdAndName/{id}/{name}")
    public Mono<CustomerResponse> findByIdAndName(@PathVariable long id, final @NonNull @PathVariable String name) {
        return service.findCustomerEntityByIdAndName(id, name);
    }

    @PostMapping("/search")
    public Flux<CustomerResponse> search(final @NonNull @RequestBody CustomerRequest request) {
        return service.search(request);
    }
}
