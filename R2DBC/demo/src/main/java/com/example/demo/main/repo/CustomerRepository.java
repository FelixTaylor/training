package com.example.demo.main.repo;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;

import com.example.demo.main.model.CustomerEntity;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@EnableR2dbcRepositories
public interface CustomerRepository extends
        R2dbcRepository<CustomerEntity, Long>,
        ReactiveQueryByExampleExecutor<CustomerEntity> {

    @Query("SELECT * FROM customer WHERE name = :name")
    Flux<CustomerEntity> findCustomerByName(String name);

    @Query("SELECT * FROM customer WHERE id = :id")
    Mono<CustomerEntity> findCustomerEntityById(Long id);

    @Query("SELECT * FROM customer WHERE id = :id AND name = :name")
    Mono<CustomerEntity> findCustomerEntityByIdAndName(Long id, String name);

}
