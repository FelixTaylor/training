CREATE SEQUENCE customer_id_seq;
CREATE TABLE customer (
    id bigint NOT NULL UNIQUE DEFAULT nextval('customer_id_seq'),
    name VARCHAR(255),
    CONSTRAINT retouren_pkey PRIMARY KEY (id)
);

ALTER TABLE customer OWNER to postgress;
ALTER SEQUENCE customer_id_seq OWNED BY customer.id;

