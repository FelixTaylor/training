from random import randint

# The Dice class represents a set of dice. The Dice.java class is used to
# calculate the ability points of a 'Dungeon & Dragons' character.
#
# Like in Ruby I don't have to implement any getter methods manually, I simply
# call dice.amount to get the amount of dice.
#
# @author: Felix Taylor
# @version: 1.1.1

class Dice(object):

    # A Dice is defined by the amount of dices which are being rolled
    # and the scope of each dice. The default set of dices is one
    # dice with a scope of six (1D6).
    def __init__(self, amount, scope):
        self.amount = amount
        self.results = [0] * amount
        self.scope = scope

    # The setAmount method sets the new amount value which defines how many dice
    # are being rolled. It also sets the results array length to the amout value
    def setAmount(self, amount):
        if amount > 0:
            self.amount = amount
            self.results = [0] * amount

    # The setScope method sets the new scope value which defines the number of
    # 'eyes' the dice have.
    def setScope(self, scope):
        if scope > 1:
            self.scope = scope

    # The eraseSmallestDice method sorts the result array and removes the
    # smallest value. Note: I do handle this method a bit different then the
    # original method in Dice.java. Here I do not have a seperate .sort method
    # implemented because I don't see the need for it.
    def eraseSmallestDice(self):
        self.results.sort()
        self.results.pop(0)

    # The roll method rolls the dice set and puts each value into the results
    # array. In this case I prefer the Ruby way to handles an array. This still
    # has all the verbose code like java.
    def roll(self):
	for x in range(self.amount):
	    self.results[x] = randint(1, self.scope)
	return self.results

    # The sum method returns the sum of the results array. In this case I
    # prefer the Python way. Here we simply call the sum() method instead of
    # calling some inject() method and giving it two parameters '0' and ':+'.
    def sum(self):
        return sum(self.results)

    # The getAmount method returns the amount of dice used by this object.
    def getAmount(self):
        return self.amount;

    # The getScrope method returns a value representing the size of the dice,
    # meaning how many numbers are represented of the dice.
    def getScope(self):
        return self.scrope;

# ========================= Test area =========================================
dice = Dice(4, 6)           # Create Dice object
dice.roll()                 # Roll the dice
dice.eraseSmallestDice()    # Erase the smallest value of the results
print dice.results          # Print results array
print dice.sum()            # Print the sum of the array values
