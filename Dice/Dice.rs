use rand::Rng;

struct Dice {
    amount: usize,
    range: i32
}

impl Dice {
    fn new(a: usize, r: i32) -> Self {
        Self {amount: a, range: r}
    }

    fn roll(&self) -> Vec<i32> {
        let mut results = vec![0; self.amount];
        for i in &mut results {
            let max = self.range + 1;
            *i = rand::thread_rng().gen_range(1..max);

        }
        results
    }

    fn sum(&self, results: &Vec<i32>) -> i32 {
        let mut sum = 0;
        for r in results {
            sum += r;
        }
        sum
    }

    fn print(&self, results: &Vec<i32>) -> () {
        let sum = self.sum(&results);
        println!("vec: {:?}; sum {}", results, sum);
    }
}

pub fn main() {
    let dice = Dice::new(2,6);
    let results = dice.roll();
    dice.print(&results);
}

