package utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * <p>
 * The <code>Dice</code> class represents a set of dices. It is used to
 * calculate the ability points of a <code>Figure</code>. The <code>Dice</code>
 * object also stores all <code>results</code> of the rolled dices for later
 * use.
 *
 * @author Felix Taylor
 * @since 0.1.0
 * @version 2.0.0
 */
public class Dice {

	private ArrayList<Integer> results;
	private int amount;
	private int range;
	private Random dice;

	public Dice() {
		this.amount = 1;
		this.range = 6;
		this.results = new ArrayList<>();
		this.dice = new Random();
	}

	public Dice(int amount, int range) {
		this();
		setAmount(amount);
		setRange(range);
	}

	public void setAmount(int amount) {
		if (amount > 0) {
			this.amount = amount;
			return;
		}
	}

	private void setRange(int range) {
		if (range > 1) {
			this.range = range;
			return;
		}
	}

	public Dice roll() {
		this.results.clear();
		for (int i = 0; i < getAmount(); i++) {
			this.results.add(this.dice.nextInt(getRange()) + 1);
		}
		return this;
	}

	public Dice sort() {
		Collections.sort(this.results);
		return this;
	}

	public Dice eraseSmallestDice() {
		sort();
		this.results.remove(0);
		return this;
	}

	public int sum() {
		return getResults().stream().mapToInt(Integer::intValue).sum();
	}

	public int getAmount() {
		return this.amount;
	}

	public int getRange() {
		return this.range;
	}

	public ArrayList<Integer> getResults() {
		return this.results;
	}

	public void clear() {
		this.results.clear();
	}
}
