"use strict";

/*
 * The Dice class represents a set of dice. The Dice.java class is used to
 * calculate the ability points of a 'Dungeon & Dragons' character.
 *
 * Note: In order to work correctly this file has to be compiled using babeljs.io
 *
 * @author: Felix Taylor
 * @version: 2.0.1
 */
class Dice {
     constructor(amount, range) {
        this._amount = amount;
        this._range = range;
        this._results = [1];
    }

    // The roll method rolls the dice set and puts each value into the
    // results array. The random function is to verbose here I like the
    // java or again the ruby way of handling this.
    roll() {
        for (let i=0; i < this._amount; i++) {
            this._results.push(Math.floor(
                Math.random() * Math.floor(this._range)
            ) + 1);
        }
    }

    // The sum method returns the sum of the result array.
    // JS also has - as many other programming languages - a reduce()
    // method which can be called to sum up the values of the results
    // array.
    sum() {
        return this._results.reduce((a,b) => a + b, 0);
    }

    // The eraseSmallestDice method first sorts the array and then removes
    // the first (smalles) value by calling the shift() method.
    // I like that this is a simple one-liner.
    eraseSmallesDice() {
        this._results.sort().shift();
    }


    // Set the amount of the Dice object. The amount defines how often the dice
    // is being rolled. The parameter value has to be greater then zero.
    set amount(amount) {
        if (amount >= 1) {
            this._amount = amount;
        }
    }

    // Set the range value of the Dice object. The range defines how many sides
    // the dice has. The parameter value has to be greater then one.
    set range(range) {
        if (range >= 2) {
            this._range = range;
        }
    }

    // Get the amount value of the current Dice object.
    get amount() {
        return this._amount;
    }

    // Get the range value of the current Dice object.
    get range() {
        return this._range;
    }
}

// ========================= Test area ========================================
let dice = new Dice(4,6);
dice.roll();                    // Roll the set of dice
dice.eraseSmallesDice();        // Erase smalles dice

console.log(dice.sum());        // Print the sum of the dice array
console.log(dice);              // Print object

