# The Dice class represents a set of dice. The Dice.java class is used to
# calculate the ability points of a 'Dungeon & Dragons' character.
#
# Note: I don't even have to import the rand() method from anywhere. In java and
# python I have to import this functionallity.
#
# @author Felix Taylor
# @version 1.1.3

class Dice

  # Ruby as a convenient way to implement all getter methods by setting the
  # 'attr_reader'. In java I have to write them manually.
  attr_reader :amount, :range, :results

  # This is the default parameterized constructor of the Dice cladss. If no
  # parameter values are provided the default values will be used.
  def initialize(amount=1,range=6)
    @amount = amount
    @range = range
    @results = []
  end

  # The roll method rolls the dice set and puts each value into the results
  # array.
  def roll
    for i in 1..@amount
      @results << rand(1..@range)
    end
  end

  # The setAmount method sets a new amount value which defines how many dice
  # are being rolled. If no value was given as parameter the method uses the
  # default value. At least one dice has to exist.
  def set_amount(amount=1)
    @amount = amount if amount > 0
  end

  # The setRange method sets a new range value which defines the number of
  # 'eyes' the dice have. If no value was given as parameter the method uses
  # the default value. Every dice has to have atleast two 'sides' (true/false)
  def set_range(range=2)
    @range = range if range > 1
  end

  # The sum method returns the sum of the result array.
  # The .inject(0 :+) method call seems to be the short version for the
  # .inject { |sum, n| sum + n } call. I also could use this method to
  # multipy the array values if I use ':*'.
  def sum
    @results.inject(0, :+)
  end

  # The eraseSmallestDice method sorts the results array and deletes the
  # smallest value. The .sort! method sorts the array in-place.
  # Note: I do handle this method a bit different then the original method in
  # Dice.java. Here I do not have a seperate .sort method implemented because
  # I don't see the need for it.
  def erase_smallest_dice
    @results.sort!
    @results = @results.drop(1)
  end
end

# ========================= Test area =========================================
# To use the getter methods of the dice class I can call 'puts dice.amount' or
# 'puts dice.range' which is very convenient.
# puts dice.amount
dice = Dice.new(4, 6)     # Create Dice object
dice.roll                 # Roll the dice
dice.erase_smallest_dice  # Erase the smallest value of the results

# The .to_s method call is usfull to print the values of the array nicely by
# adding '[' and ']' to the output.
# puts dice.results.to_s
puts dice.results.to_s  # Print results array
puts dice.sum           # Print the sum of the array values

