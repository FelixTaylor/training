#!/usr/bin/env ruby

# @version       1.0.0
# @author        Felix Taylor
# @since         2021-03-10
# @last updated  2021-03-10
# @licence       GPLv3
#
# @info

class hello_clazz
    def initialize
        # todo
    end

    def greet
      puts 'Hello World!'
    end
end


clazz = hello_clazz.new
clazz.greet
