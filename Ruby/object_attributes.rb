#!/usr/bin/env ruby

# @version       1.0.0
# @author        Felix Taylor
# @since         2021-03-11
# @last updated  2021-03-11
# @licence       GPLv3
#
# @info

class Object_attributes

    def initialize(name)
        @name = name
    end

    def name
      @name
    end

    def number=(num)
      @number = num
    end

    def number
      @number
    end
end

o = Object_attributes.new('Rubikus')
puts o.name

o.number = 100
puts o.number
