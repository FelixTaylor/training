pub trait Sound {
    fn make_sound(&self) -> &str;
}
