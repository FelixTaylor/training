use crate::sound::Sound;

#[derive(Debug, Clone)]
pub struct Sheep {
    id: i64,
    name: String,
    sound: String,
}

impl Sheep {
    pub fn new(id: i64, name: String, sound: String) -> Sheep {
        Sheep { id, name, sound }
    }

    fn id(&self) -> i64 {
        self.id
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn sound(&self) -> &str {
        &self.sound
    }
}

impl Sound for Sheep {
    fn make_sound(&self) -> &str {
        &self.sound()
    }
}
