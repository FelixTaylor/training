mod sheep;
use sheep::Sheep;

mod sound;
use sound::Sound;

mod herd;
use herd::Herd;

fn main() {
    let mut herd = Herd::new();
    herd.push(Sheep::new(1, "Henry".to_string(), "Baaaah!".to_string()));
    herd.push(Sheep::new(2, "Ralf".to_string(), "Meeh.".to_string()));

    for sheep in herd {
        println!("{}: {}", sheep.name(), sheep.make_sound());
    }
}
