use crate::sheep::Sheep;

#[derive(Debug)]
pub struct Herd {
    idx: usize,
    sheeps: Vec<Sheep>,
}

impl Herd {
    pub fn new() -> Herd {
        Herd {
            idx: 0,
            sheeps: Vec::new(),
        }
    }

    pub fn push(&mut self, sheep: Sheep) {
        self.sheeps.push(sheep);
    }
}

impl Iterator for Herd {
    type Item = Sheep;

    fn next(&mut self) -> Option<Self::Item> {
        if self.idx < self.sheeps.len() {
            // todo get reference instead of cloning.
            let output = self.sheeps[self.idx].clone();
            self.idx += 1;
            Some(output)
        } else {
            None
        }
    }
}
